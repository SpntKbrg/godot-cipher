extends LineEdit

export (String) var target = 'A'
export (int) var targ_id = 0
func _ready():
  self.get_node("replace_label").text = target
  pass
  
func get_replace():
  if (not self.text.empty()):
    return self.text.ord_at(0)
  else :
    return target.ord_at(0)