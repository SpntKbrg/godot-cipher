extends GridContainer

export (PackedScene) var replace_field
export (int) var start = 0
export (int) var end = 52
func _ready():
  for i in range(start, end):
    var current = replace_field.instance()
    current.add_to_group("replacement")
    var character
    if i < 26:
      character = PoolByteArray([i + 65])
    else :
      character = PoolByteArray([i - 26 + 97])
    current.target = character.get_string_from_utf8()
    current.targ_id = i
    add_child(current)
  pass


func _on_case_check_toggled( button_pressed ):
  for x in self.get_children():
    if x.targ_id >= 26:
      x.editable = button_pressed
