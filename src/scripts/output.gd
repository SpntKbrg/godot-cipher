extends TextEdit

func _ready():
  set_process(true)


func _on_submit_button_pressed():
  var temp = self.get_parent().get_node("input_edit").text.to_utf8()
  var result = PoolByteArray()
  var sensitive = self.get_parent().get_node("case_check").pressed
  for x in temp.size():
    if temp[x] < 91 and temp[x] > 64:
      var target = self.get_parent().get_node("replacement_grid").get_child(temp[x] - 65).get_replace()
      result.append(target)
    elif temp[x] > 96 and temp[x] < 123 and sensitive:
      var target = self.get_parent().get_node("replacement_grid_lower").get_child(temp[x] - 97).get_replace()
      result.append(target)
    elif temp[x] > 96 and temp[x] < 123:
      var target = self.get_parent().get_node("replacement_grid").get_child(temp[x] - 97).get_replace()
      result.append(target)
    else :
      result.append(temp[x])
  self.text = result.get_string_from_utf8()
